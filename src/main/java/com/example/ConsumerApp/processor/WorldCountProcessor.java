package com.example.ConsumerApp.processor;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class WorldCountProcessor {

    static final Serde<String> STRING_SERDE = Serdes.String();

    @Value(value = "${topic.name}")
    private String topicName;

    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder) {
//        KStream<String, String> messageStream = streamsBuilder
//                .stream(topicName, Consumed.with(STRING_SERDE, STRING_SERDE));
//        GlobalKTable<String, Long> worldCount = messageStream
//                .flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
//                .groupBy((key, value) -> value, Grouped.with(STRING_SERDE, STRING_SERDE))
//                .count(Materialized.as("counts"));
        GlobalKTable<String, String> firstKTable = streamsBuilder
                .globalTable(
                            topicName,
                            Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as("ktable")
                                    .withKeySerde(STRING_SERDE)
                                    .withValueSerde(STRING_SERDE));
    }
}
