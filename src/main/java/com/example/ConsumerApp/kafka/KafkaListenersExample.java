package com.example.ConsumerApp.kafka;

import com.example.ConsumerApp.dto.MyMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListenersExample {

    @Value(value = "${topic.name}")
    private String topicName;

    @Value(value = "${spring.kafka.consumer.group-id}")
    private String groupId;

    @KafkaListener(
            topics = "my-topic",
            groupId = "my-group",
            containerFactory = "kafkaListenerContainerFactory")
    public void listenGroupFoo(String msg) {
        System.out.println("Received Message in group foo: " + msg);
    }
}
